export interface AuthenticationInterface {
  id?: number;
  email: string;
  password: string;
  firstName?: string;
  lastName?: string;
  refreshToken?: string;
}
