import { Router } from "express";
import { AuthController } from "./resources/authentication/authentication.controller";

const APIROUTES = Router();

APIROUTES.get("/", (req, res) => {
  return res.send({ message: "API docs" });
});

APIROUTES.use("/auth", AuthController);

export { APIROUTES };
