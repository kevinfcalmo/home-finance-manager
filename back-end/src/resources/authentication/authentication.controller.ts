import { Router, Request, Response } from "express";
import { isEmailvalid } from "../../utils/isEmailValid";
import { isPasswordValid } from "../../utils/isPasswordValid";
import { AuthenticationClass } from "./authentication.service";
import speakeasy from "speakeasy";
import { prisma } from "../../libs/prisma";

const AuthController = Router();

interface RequestBody {
  email: string;
  password: string;
}

AuthController.post("/register", async (req: Request, res: Response) => {
  const { lastName, firstName, email, password, confirmPassword } = req.body;
  if (!email || !password || !lastName || !firstName || !confirmPassword) {
    return res.status(400).send({ message: "all fields are required" });
  }
  if (
    !isEmailvalid(email) ||
    !isPasswordValid(password) ||
    !isPasswordValid(confirmPassword)
  ) {
    return res.status(403).send({
      message: "email or password not match to rules",
    });
  }

  if (password !== confirmPassword) {
    return res.status(403).send({
      message: "password  confirmPassword not match",
    });
  }

  const IsUserRegister = await AuthenticationClass.register({
    email,
    password,
    lastName,
    firstName,
  });
  let message = IsUserRegister.message;
  if (!IsUserRegister.success) {
    return res.status(401).send({ message });
  }
  return res.status(200).send({ message });
});

AuthController.post("/login", async (req: Request, res: Response) => {
  const { email, password }: RequestBody = req.body;

  if (!email || !password) {
    return res.status(400).send({
      message: "email / password is required",
    });
  }

  const isUserLogin = await AuthenticationClass.login({ email, password });
  const message = isUserLogin.message;
  const data = isUserLogin.data;
  if (!isUserLogin.success) {
    return res.status(403).send({ message });
  }
  res.cookie("jwt", isUserLogin.data.refreshToken, {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000, //one day max
    sameSite: "none",
    secure: true,
  });
  return res.status(200).send({
    message,
    data,
  });
});

AuthController.post("/refreshToken", async (req: Request, res: Response) => {
  const cookies = req.cookies;

  if (!cookies?.jwt) {
    return res.status(401).send({ message: "cookie jwt not found" });
  }

  const refreshToken: string = cookies.jwt;
  const isTokenRefresh = await AuthenticationClass.refreshToken(refreshToken);
  let message = isTokenRefresh?.message;
  if (!isTokenRefresh?.success) {
    return res.status(400).send({ message });
  }
  res.cookie("jwt", isTokenRefresh.data.refreshToken, {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000,
    sameSite: "none",
    secure: true,
  });
  return res.status(200).send({ message, data: isTokenRefresh.data });
});

AuthController.post("/logout", async (req: Request, res: Response) => {
  const cookies = req.cookies;

  if (!cookies?.jwt) {
    return res.status(200).send({ message: "user logged out without cookie" });
  }
  const isLoggedOutUser = await AuthenticationClass.logout(cookies?.jwt);
  res.clearCookie("jwt", {
    httpOnly: true,
    maxAge: 24 * 60 * 60 * 1000,
    sameSite: "none",
    secure: true,
  });
  return res.status(204).send({ message: isLoggedOutUser.message });
});

AuthController.post("/resetLink", async (req: Request, res: Response) => {
  const { email } = req.body;
  if (!email) {
    res.status(400).send({ message: "email address not found" });
  }
  const { success, message, status } = await AuthenticationClass.resetLink(
    email
  );
  return res.status(status).send({ message });
});

AuthController.post("/resetPassword", async (req: Request, res: Response) => {
  const token: string = req.query.token as string;
  if (!token) {
    return res.status(403).send({ message: "token not found" });
  }

  const { password, passwordBis } = req.body;
  if (!password || !passwordBis) {
    return res
      .status(403)
      .send({ message: "password/passwordBis is required" });
  }

  if (!isPasswordValid(password) || !isPasswordValid(password)) {
    return res
      .status(403)
      .send({ message: "password/passwordBis is not valid" });
  }

  const isPasswordReset = await AuthenticationClass.resetPassword(
    token,
    password,
    passwordBis
  );
  const { status, message } = isPasswordReset;
  return res.status(status).send({ message });
});

AuthController.post("/enabled2FA", async (req: Request, res: Response) => {
  const { id } = req.body;

  if (!id) {
    return res.status(500).send({ enabled: false, message: "id is required" });
  }
  const { status, success, message, data } =
    await AuthenticationClass.enabled2FA(id);
  return res.status(status).send({ enabled: success, message, secret: data });
});

AuthController.post("/verify2FA", async (req: Request, res: Response) => {
  const { id, token } = req.body;

  if (!token || !id) {
    return res.status(500).send("token/id is required");
  }

  const { status, success, message } = await AuthenticationClass.verify2FA(
    id,
    token
  );
  return res.status(status).send({ verified: success, message });
});

AuthController.post(
  "/validate2FAToken",
  async (req: Request, res: Response) => {
    const { id, token } = req.body;

    if (!token || !id) {
      return res.status(500).send({ message: "token is required" });
    }
    const { status, success, message } =
      await AuthenticationClass.validate2FAToken(id, token);
    return res.status(status).send({ verified: success, message });
  }
);

export { AuthController };
