import { JwtPayload } from "jsonwebtoken";
import { generateJWTAcessToken, generateJWTRefreshToken } from "../../libs/jwt";
import { prisma } from "../../libs/prisma";
import { AuthenticationInterface } from "../../types/authentication";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import crypto from "crypto";
import speakeasy from "speakeasy";
import { sendEmail } from "../../libs/nodemailer";

interface ExportResult {
  action: string;
  success: boolean;
  message: string;
  data?: any;
}

export class AuthenticationClass {
  static async login({ email, password }: AuthenticationInterface) {
    const fetchUser = await prisma.users.findUnique({
      where: { email },
    });
    if (!fetchUser) {
      let exportResult: ExportResult = {
        action: "login",
        success: false,
        message: "email not found",
      };
      return exportResult;
    }

    let matchPassword = await bcrypt.compare(password, fetchUser.password);
    if (!matchPassword) {
      let exportResult: ExportResult = {
        action: "login",
        success: false,
        message: "password not match",
      };
      return exportResult;
    }

    const { id } = fetchUser;
    let accessToken = generateJWTAcessToken({
      id,
      email,
    } as JwtPayload);
    let refreshToken = generateJWTRefreshToken({
      id,
      email,
    } as JwtPayload);

    await prisma.users.update({
      where: { email },
      data: {
        refreshToken,
      },
    });

    const userProfile = await prisma.profiles.findUnique({
      where: { userId: id },
    });

    let exportResult: ExportResult = {
      action: "login",
      success: true,
      message: `user ${email} is logged in`,
      data: {
        id,
        email,
        accessToken,
        refreshToken,
        lastName: userProfile?.lastName,
        firstName: userProfile?.firstName,
      },
    };
    return exportResult;
  }

  static async register({
    email,
    password,
    lastName,
    firstName,
  }: AuthenticationInterface) {
    let foundUser = await prisma.users.findUnique({
      where: { email },
    });
    if (foundUser) {
      return {
        action: "register",
        success: false,
        message: "user already exist",
      };
    }
    const hashPassword = await bcrypt.hash(password, 10);
    const user = await prisma.users.create({
      data: {
        email,
        password: hashPassword,
      },
    });
    await prisma.profiles.create({
      data: {
        firstName: firstName!,
        lastName: lastName!,
        userId: user.id,
      },
    });
    return {
      action: "register",
      success: true,
      message: "user register",
    };
  }

  static async refreshToken(refreshToken: string) {
    let fetchUserByRefreshToken = await prisma.users.findFirst({
      where: { refreshToken },
    });
    if (!fetchUserByRefreshToken) {
      return {
        action: "refreshToken",
        success: false,
        message: "refreshToken not found",
      };
    }
    const promise: ExportResult = await new Promise((resolve, reject) => {
      jwt.verify(
        refreshToken!,
        process.env.REFRESH_TOKEN_SECRET!,
        async (err, decoded: any) => {
          if (err || fetchUserByRefreshToken?.email !== decoded.email) {
            resolve({
              action: "refreshToken",
              success: false,
              message: "token invalid",
            });
          }

          const accessToken = generateJWTAcessToken({
            id: fetchUserByRefreshToken!.id,
            username: fetchUserByRefreshToken!.email,
          } as JwtPayload);
          const newRefreshToken = generateJWTRefreshToken({
            id: fetchUserByRefreshToken!.id,
            username: fetchUserByRefreshToken!.email,
          } as JwtPayload);
          await prisma.users.update({
            where: { email: fetchUserByRefreshToken!.email },
            data: {
              refreshToken: newRefreshToken,
            },
          });

          resolve({
            action: "refreshToken",
            success: true,
            message: "RefreshToken is refresh",
            data: {
              id: fetchUserByRefreshToken!.id,
              email: fetchUserByRefreshToken!.email,
              accessToken,
              refreshToken: newRefreshToken,
            },
          });
        }
      );
    });

    return promise;
  }

  static async logout(refreshToken: string) {
    const fetchUserByRefreshToken = await prisma.users.findFirst({
      where: { refreshToken },
    });

    if (!fetchUserByRefreshToken) {
      return {
        action: "logout",
        success: true,
        message: "user logged out",
      };
    }
    await prisma.users.update({
      where: { email: fetchUserByRefreshToken.email },
      data: {
        refreshToken: null,
      },
    });
    return {
      action: "logout",
      success: true,
      message: "user logged out",
    };
  }

  static async resetLink(email: string) {
    const findUserByEmail = await prisma.users.findUnique({ where: { email } });
    if (!findUserByEmail) {
      return {
        action: "resetLink",
        status: 403,
        success: false,
        message: "user not found",
      };
    }
    const randomToken = crypto.randomBytes(64).toString("hex");
    await prisma.users.update({
      where: { email },
      data: {
        resetPasswordToken: randomToken,
      },
    });
    await sendEmail({
      from: "local@homefinancemanager.com",
      to: email,
      subject: "Réinitialisation de mot de passe",
      html: `<a href="http://localhost:5173/reinitialiser-mot-de-passe?token=${randomToken}" >Reset your password with this link</a>`,
    });
    return {
      action: "resetLink",
      status: 200,
      success: true,
      message: "reset link send",
    };
  }

  static async resetPassword(
    token: string,
    password: string,
    passwordBis: string
  ) {
    if (password !== passwordBis) {
      return {
        action: "resetPassword",
        status: 403,
        success: false,
        message: "passwordpasswordBis not match",
      };
    }
    let findUserWithToken = await prisma.users.findFirst({
      where: { resetPasswordToken: token },
    });

    if (!findUserWithToken) {
      return {
        action: "resetPassword",
        status: 403,
        success: false,
        message: "user not found",
      };
    }

    const hashPassword = await bcrypt.hash(password, 10);
    await prisma.users.update({
      where: { email: findUserWithToken.email },
      data: {
        password: hashPassword,
        resetPasswordToken: null,
      },
    });
    return {
      action: "resetPassword",
      status: 200,
      success: true,
      message: "password reset",
    };
  }

  static async enabled2FA(id: number) {
    const user = await prisma.users.findUnique({ where: { id: id } });
    if (!user) {
      return {
        action: "enabled2FA",
        status: 404,
        success: false,
        message: "user not found",
      };
    }
    if (user?.otpBase32 !== null) {
      return {
        action: "enabled2FA",
        status: 400,
        success: false,
        message: "2FA Base32 already exist",
      };
    }
    const { ascii, hex, base32, otpauth_url } = speakeasy.generateSecret();
    await prisma.users.update({
      where: { id: id },
      data: {
        otpAscii: ascii,
        otpHex: hex,
        otpBase32: base32,
        otpAuthUrl: otpauth_url,
      },
    });
    return {
      action: "enabled2FA",
      status: 200,
      success: true,
      message: "Secret Key (base32) is created",
      data: {
        secret: base32,
      },
    };
  }

  static async verify2FA(id: number, token: string) {
    const user = await prisma.users.findUnique({ where: { id } });
    if (!user) {
      return {
        action: "verify2FA",
        status: 404,
        success: false,
        message: "user not found",
      };
    }
    const verified = speakeasy.totp.verify({
      secret: user?.otpBase32!,
      encoding: "base32",
      token,
    });
    if (verified) {
      await prisma.users.update({
        where: { id },
        data: {
          otpEnabled: true,
          otpVerified: true,
        },
      });

      return {
        action: "verify2FA",
        status: 200,
        success: true,
        message: "2FA enabled and verify",
      };
    } else {
      return {
        action: "verify2FA",
        status: 403,
        success: false,
        message: "token invalid",
      };
    }
  }

  static async validate2FAToken(id: number, token: string) {
    const user = await prisma.users.findUnique({ where: { id } });
    if (!user) {
      return {
        action: "verify2FA",
        status: 404,
        success: false,
        message: "user not found",
      };
    }
    const tokenValidates = speakeasy.totp.verify({
      secret: user?.otpBase32!,
      encoding: "base32",
      token,
      window: 1,
    });
    if (tokenValidates) {
      return {
        action: "validate2FAToken",
        status: 200,
        success: true,
        message: "token is valid",
      };
    } else {
      return {
        action: "verify2FA",
        status: 500,
        success: false,
        message: "invalid token",
      };
    }
  }
}
