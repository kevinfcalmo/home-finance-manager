export function isPasswordValid(password: string) {
  let passwordRegex =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*(),.?":{}|<>]).{12,}$/;
  return passwordRegex.test(password);
}
