import nodemailer from "nodemailer";
const transporter = nodemailer.createTransport({
  port: 1025,
  host: "localhost",
  secure: false,
  tls: {
    rejectUnauthorized: false,
  },
});

type SendEmailType = {
  from: string;
  to: string;
  subject: string;
  html: string;
};

export async function sendEmail({ from, to, subject, html }: SendEmailType) {
  const mailOptions = {
    from,
    to,
    subject,
    html,
  };

  await transporter.sendMail(mailOptions);
}
