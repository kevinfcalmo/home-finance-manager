import express, { Express, Request, Response, Application } from "express";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";
import { APIROUTES } from "./src/api_routes";
import cors from "cors";
import { credentials } from "./src/middlewares/credentials.handler";
import { corsOptions } from "./src/config/corsOptions";

dotenv.config();

export const app: Application = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(cookieParser());
app.use(credentials);
app.use(cors(corsOptions));

app.get("/", (req: Request, res: Response) => {
  res.send({ message: "Hello from Express" });
});

app.use("/api", APIROUTES);

app.listen(port, () => {
  console.log("Silence, ça tourne!");
});
