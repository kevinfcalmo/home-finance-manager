import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";

describe("Testing route /api/auth/resetPassword", () => {
  const url = "/api/auth/resetPassword";
  const passwords = { password: "password", passwordBis: "newPassword" };
  it("Testing route without password and token", async () => {
    const resetPasswordRequest = await request(app).post(url);
    expect(resetPasswordRequest.status).toBe(403);
    expect(resetPasswordRequest.body.message).toBe("token not found");
  });
  it("Testing route without password", async () => {
    const requestURL = url + "?token='helloworld'";
    const resetPasswordRequest = await request(app).post(requestURL);
    expect(resetPasswordRequest.status).toBe(403);
    expect(resetPasswordRequest.body.message).toBe(
      "password/passwordBis is required"
    );
  });
  it("Testing route with false passwords", async () => {
    const requestURL = url + "?token='helloworld'";
    const resetPasswordRequest = await request(app)
      .post(requestURL)
      .send(passwords);
    expect(resetPasswordRequest.status).toBe(403);
    expect(resetPasswordRequest.body.message).toBe(
      "password/passwordBis is not valid"
    );
  });
  it("Testing route with false passwords", async () => {
    const requestURL = url + "?token='helloworld'";
    const resetPasswordRequest = await request(app)
      .post(requestURL)
      .send(passwords);
    expect(resetPasswordRequest.status).toBe(403);
    expect(resetPasswordRequest.body.message).toBe(
      "password/passwordBis is not valid"
    );
  });
  it("Testing route with real passwords but differents passwords", async () => {
    const requestURL = url + "?token='helloworld'";
    const resetPasswordRequest = await request(app)
      .post(requestURL)
      .send({ password: "@Password123456", passwordBis: "@Password1234567" });
    expect(resetPasswordRequest.status).toBe(403);
    expect(resetPasswordRequest.body.message).toBe(
      "passwordpasswordBis not match"
    );
  });
  it("Testing route with real passwords but false token", async () => {
    const requestURL = url + "?token='helloworld'";
    const resetPasswordRequest = await request(app)
      .post(requestURL)
      .send({ password: "@Password123456", passwordBis: "@Password123456" });
    expect(resetPasswordRequest.status).toBe(403);
    expect(resetPasswordRequest.body.message).toBe("user not found");
  });
  /* Impossible to test that everything is fine with a test, manual test required */
});
