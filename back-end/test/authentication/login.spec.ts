import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";

describe("Testing /api/auth/login endpoint", () => {
  it("testing request without credential", async () => {
    const response = await request(app).post("/api/auth/login").send({});
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("email / password is required");
    request(app).off;
  });
  it("testing request with false email address", async () => {
    const response = await request(app)
      .post("/api/auth/login")
      .send({ email: "email@test.com", password: "password" });
    expect(response.status).toBe(403);
    expect(response.body.message).toBe("email not found");
    request(app).off;
  });
  it("testing request with false password", async () => {
    const response = await request(app)
      .post("/api/auth/login")
      .send({ email: "test@test.com", password: "password" });
    expect(response.status).toBe(403);
    expect(response.body.message).toBe("password not match");
    request(app).off;
  });
  it("testing request with real user credential", async () => {
    const response = await request(app)
      .post("/api/auth/login")
      .send({ email: "test@test.com", password: "@Password123456" });
    expect(response.status).toBe(200);
    expect(response.body.message).toBe("user test@test.com is logged in");
    expect(response.body.data.hasOwnProperty("id")).toBe(true);
    expect(response.body.data.hasOwnProperty("email")).toBe(true);
    expect(response.body.data.hasOwnProperty("accessToken")).toBe(true);
    expect(response.body.data.hasOwnProperty("refreshToken")).toBe(true);
    request(app).off;
  });
});

/* id,
        email,
        accessToken,
        refreshToken, */
