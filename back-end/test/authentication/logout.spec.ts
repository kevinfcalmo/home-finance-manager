import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";
import { prisma } from "../../src/libs/prisma";

describe("Tesing logout request", () => {
  const url = "/api/auth/logout";
  const url_login = "/api/auth/login";
  const loginCredentials = {
    email: "test@test.com",
    password: "@Password123456",
  };
  it("testing logout request without cookies", async () => {
    const logoutRequest = await request(app).post(url);
    expect(logoutRequest.status).toBe(200);
    request(app).off;
  });
  it("testing logout request in real condition", async () => {
    const loginRequest = await request(app)
      .post(url_login)
      .send(loginCredentials);
    const { refreshToken } = loginRequest.body.data;
    const logoutRequest = await request(app)
      .post(url)
      .set("Cookie", `jwt=${refreshToken}`);
    expect(logoutRequest.status).toBe(204);
  });
});
