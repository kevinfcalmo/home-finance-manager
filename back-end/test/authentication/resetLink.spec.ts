import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";
import { prisma } from "../../src/libs/prisma";

describe("testind /api/auth/resetLink", () => {
  const url = "/api/auth/resetLink";
  it("testing resetLink request without email", async () => {
    const resetLinkRequest = await request(app).post(url);
    expect(resetLinkRequest.status).toBe(400);
    expect(resetLinkRequest.body.message).toBe("email address not found");
  });
  it("testing resetLink request with false email", async () => {
    const resetLinkRequest = await request(app)
      .post(url)
      .send({ email: "hello@hello.com" });
    expect(resetLinkRequest.status).toBe(403);
    expect(resetLinkRequest.body.message).toBe("user not found");
    request(app).off;
  });
  it("testing resetLink request with real email", async () => {
    const resetLinkRequest = await request(app)
      .post(url)
      .send({ email: "test@test.com" });
    expect(resetLinkRequest.status).toBe(200);
    expect(resetLinkRequest.body.message).toBe("reset link send");
    request(app).off;
  });
});
