import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";

describe("Testing route /api/auth/validate2FAToken", () => {
  const url = "/api/auth/validate2FAToken";
  it("testing without id or token", async () => {
    const validate2FATokenRequest = await request(app).post(url);
    expect(validate2FATokenRequest.status).toBe(500);
    expect(validate2FATokenRequest.body.message).toBe("token is required");
  });
  it("testing with wrong id", async () => {
    const validate2FATokenRequest = await request(app)
      .post(url)
      .send({ id: 10, token: "123456" });
    expect(validate2FATokenRequest.status).toBe(404);
    expect(validate2FATokenRequest.body.message).toBe("user not found");
    expect(validate2FATokenRequest.body.verified).toBe(false);
  });
  it("testing with wrong token", async () => {
    const validate2FATokenRequest = await request(app)
      .post(url)
      .send({ id: 4, token: "123456" });
    expect(validate2FATokenRequest.status).toBe(500);
    expect(validate2FATokenRequest.body.message).toBe("invalid token");
    expect(validate2FATokenRequest.body.verified).toBe(false);
  });
});
