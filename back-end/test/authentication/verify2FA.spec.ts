import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";

describe("Testing route /api/auth/verify2FA", () => {
  const url = "/api/auth/verify2FA";
  const user = {
    id: 4,
    email: "test@test.com",
    password: "@Password123456",
  };
  it("testing without id and token", async () => {
    const enabled2FARequest = await request(app).post(url);
    expect(enabled2FARequest.status).toBe(500);
  });
  it("testing with wrong id and token", async () => {
    const enabled2FARequest = await request(app)
      .post(url)
      .send({ id: 10, token: "123456" });
    expect(enabled2FARequest.status).toBe(404);
    expect(enabled2FARequest.body.message).toBe("user not found");
  });
  it("testing with wrong token", async () => {
    const enabled2FARequest = await request(app)
      .post(url)
      .send({ id: user.id, token: "123456" });
    expect(enabled2FARequest.status).toBe(403);
    expect(enabled2FARequest.body.message).toBe("token invalid");
  });
  /* Impossible to test anything else, manual test required */
});
