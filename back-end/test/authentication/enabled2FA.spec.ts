import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";

describe("Testing route /api/auth/enabled2FA", () => {
  const url = "/api/auth/enabled2FA";
  const user = {
    id: 4,
    email: "test@test.com",
    password: "@Password123456",
  };
  it("testing without id", async () => {
    const enabled2FARequest = await request(app).post(url);
    expect(enabled2FARequest.status).toBe(500);
    expect(enabled2FARequest.body.message).toBe("id is required");
  });

  /* Impossible to test anything else, manual test required */
});
