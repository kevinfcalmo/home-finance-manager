import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";
import { prisma } from "../../src/libs/prisma";

const URL = "/api/auth/register";

describe("Testing /api/auth/register endpoint", () => {
  it("Testing request without credential", async () => {
    const response = await request(app).post(URL).send({});
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("email / password is required");
  });
  it("Testing request if user is not valid", async () => {
    const response = await request(app)
      .post(URL)
      .send({ email: "johndoe@doe.com", password: "password" });
    expect(response.status).toBe(403);
    expect(response.body.message).toBe("email or password not match to rules");
  });
  it("Testing request when email already exist", async () => {
    const response = await request(app)
      .post(URL)
      .send({ email: "johndoe@test.com", password: "@Password1290" });
    expect(response.status).toBe(401);
    expect(response.body.message).toBe("user already exist");
  });
  it("Testing api when all email and password are good", async () => {
    const response = await request(app)
      .post(URL)
      .send({ email: "test2@test.com", password: "@Password123456" });
    expect(response.status).toBe(200);
    expect(response.body.message).toBe("user register");
  });
  after(async () => {
    await prisma.users.delete({ where: { email: "test2@test.com" } });
    await prisma.$disconnect();
  });
});
