import request from "supertest";
import { expect } from "expect";
import { app } from "../../index";
import { prisma } from "../../src/libs/prisma";

describe("Testing request to refresh jwt token", () => {
  const url = "/api/auth/refreshToken";
  const url_login = "/api/auth/login";
  const loginCredentials = {
    email: "test@test.com",
    password: "@Password123456",
  };
  it("testing without cookie", async () => {
    const response = await request(app).post(url).send({});
    expect(response.status).toBe(401);
    expect(response.body.message).toBe("cookie jwt not found");
    request(app).off;
  });
  it("Testing with a refresh token different", async () => {
    const loginRequest = await request(app)
      .post(url_login)
      .send(loginCredentials);
    const { refreshToken } = loginRequest.body.data;
    request(url_login).off;
    const refreshTokenRequest = await request(app)
      .post(url)
      .set("Cookie", `jwt=${refreshToken}123456`);
    expect(refreshTokenRequest.status).toBe(400);
    expect(refreshTokenRequest.body.message).toBe("refreshToken not found");
    await prisma.users.update({
      where: { email: loginCredentials.email },
      data: {
        refreshToken: null,
      },
    });
  });
  it("Testing with valid refreshToken", async () => {
    const loginRequest = await request(app)
      .post(url_login)
      .send(loginCredentials);
    const { refreshToken } = loginRequest.body.data;
    request(url_login).off;
    const refreshTokenRequest = await request(app)
      .post(url)
      .set("Cookie", `jwt=${refreshToken}`);
    expect(refreshTokenRequest.status).toBe(200);
    expect(refreshTokenRequest.body.message).toBe("RefreshToken is refresh");
    expect(refreshTokenRequest.body.data.hasOwnProperty("id")).toBe(true);
    expect(refreshTokenRequest.body.data.hasOwnProperty("email")).toBe(true);
    expect(refreshTokenRequest.body.data.hasOwnProperty("accessToken")).toBe(
      true
    );
    expect(refreshTokenRequest.body.data.hasOwnProperty("refreshToken")).toBe(
      true
    );
    await prisma.users.update({
      where: { email: loginCredentials.email },
      data: {
        refreshToken: null,
      },
    });
  });
});
