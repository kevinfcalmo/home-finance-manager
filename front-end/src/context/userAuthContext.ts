import { createContext, useContext } from "react";

interface UserContextType {
  cookies: any; // Define the type for cookies here
  login: (arg: any) => Promise<any>; // Define the type for the argument here
  logout: () => void;
  register: (arg: any) => Promise<any>;
  forgotPassword: (email: string) => Promise<any>;
  resetPassword: (
    password: string,
    confirmPassword: string,
    token: string
  ) => Promise<any>;
  loading: boolean;
}

export const UserAuthContext = createContext<UserContextType | undefined>(
  undefined
);

export const useAuth = () => {
  return useContext(UserAuthContext);
};
