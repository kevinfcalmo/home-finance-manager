import { useMemo, useState } from "react";
import { UserAuthContext } from "./userAuthContext";
import axios from "../lib/axios.ts";
import { useCookies } from "react-cookie";

export default function UserAuthProvider({ children }: any) {
  const [cookies, setCookies, removeCookie] = useCookies();
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const register = async ({
    lastName,
    firstName,
    email,
    password,
    confirmPassword,
  }: any) => {
    setLoading(true);
    try {
      const res = await axios.post("/api/auth/register", {
        firstName,
        lastName,
        email,
        password,
        confirmPassword,
      });
      if (res.status === 200) {
        setLoading(false);
        return {
          isError: false,
          message: "",
        };
      }
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      return {
        isError: true,
        message: error.response.data.message,
      };
    }
  };

  const login = async ({ email, password }: any) => {
    setLoading(true);
    try {
      const res = await axios.post("/api/auth/login", {
        email: email,
        password: password,
      });
      if (res.status === 200) {
        setIsError(false);
        setErrorMessage("");
        setCookies("token", res.data.data.accessToken); // your token
        setCookies("jwt", res.data.data.refreshToken); //refreshToken
        setCookies("email", res.data.data.email); // optional data
        setCookies("id", res.data.data.id);
        setCookies("lastName", res.data.data.lastName);
        setCookies("firstName", res.data.data.firstName);
        setLoading(false);
        return {
          isError: false,
          message: "",
        };
      }
    } catch (error: any) {
      setIsError(true);
      setErrorMessage(error.response.data.message);
      setLoading(false);
      return {
        isError: true,
        message: error.response.data.message,
      };
    }
  };

  const logout = async () => {
    await axios.post("/api/auth/logout");
    ["token", "name"].forEach((obj) => removeCookie(obj)); // remove data save in cookies
  };

  const forgotPassword = async (email: string) => {
    setLoading(true);
    try {
      const response = await axios.post("/api/auth/resetLink", { email });
      if (response.status === 200) {
        setLoading(false);
        return {
          isError: false,
          message: "",
        };
      }
    } catch (error: any) {
      setLoading(false);
      return {
        isError: true,
        message: error.response.data.message,
      };
    }
  };

  const resetPassword = async (
    password: string,
    confirmPassword: string,
    token: string
  ) => {
    setLoading(true);
    try {
      const response = await axios.post(
        `/api/auth/resetPassword?token=${token}`,
        {
          password,
          passwordBis: confirmPassword,
        }
      );
      if (response.status === 200) {
        return {
          isError: false,
          message: "",
        };
      }
    } catch (error: any) {
      return {
        isError: true,
        message: error.response.data.message,
      };
    }
  };

  const value = useMemo(
    () => ({
      cookies,
      login,
      logout,
      register,
      forgotPassword,
      resetPassword,
      loading,
    }),
    [cookies]
  );
  return (
    <UserAuthContext.Provider value={value}>
      {children}
    </UserAuthContext.Provider>
  );
}
