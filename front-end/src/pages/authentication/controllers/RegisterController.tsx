import { useForm } from "react-hook-form";
import RegisterView from "../views/RegisterView";
import { useAuth } from "../../../context/userAuthContext";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const RegisterController = () => {
  const [isError, setIsError] = useState(false);
  const [isErrorMessage, setIsErrorMessage] = useState("");

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const auth = useAuth();
  const navigate = useNavigate();

  const onSubmit = async (data: any) => {
    const register = await auth?.register(data);
    if (!register.isError) {
      await auth?.login({ email: data.email, password: data.password });
      navigate("/");
    } else {
      setIsError(true);
      setIsErrorMessage(register.message);
    }
  };
  return (
    <RegisterView
      register={register}
      handleSubmit={handleSubmit}
      watch={watch}
      errors={errors}
      onSubmit={onSubmit}
      loading={auth!.loading}
      isError={isError}
      errorMessage={isErrorMessage}
    />
  );
};

export default RegisterController;
