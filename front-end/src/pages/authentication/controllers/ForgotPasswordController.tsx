import { useState } from "react";
import ForgotPasswordView from "../views/ForgotPasswordView";
import { useForm } from "react-hook-form";
import { useAuth } from "../../../context/userAuthContext";
import { Box, Center, Heading, Text, Link } from "@chakra-ui/react";
import { CheckCircleIcon } from "@chakra-ui/icons";

const ForgotPasswordController = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const authentication = useAuth();
  const [isLoading, setIsLoading] = useState(authentication?.loading);
  const [isError, setIsError] = useState(false);
  const [isErrorMessage, setIsErrorMessage] = useState(false);
  const [isLinkSend, setIsLinkSend] = useState(false);

  const onSubmit = async (data: { email: string }) => {
    const sendResetPassword = await authentication?.forgotPassword(data.email);
    setIsError(sendResetPassword.isError);
    setIsErrorMessage(sendResetPassword.message);
    console.log(sendResetPassword)
    if (!sendResetPassword.isError) {
      setIsLinkSend(true);
    }
  };

  if (isLinkSend) {
    return (
      <Center h="100vh">
        <Box textAlign="center" py={10} px={6}>
          <CheckCircleIcon boxSize={"50px"} color={"green.500"} />
          <Heading as="h2" size="xl" mt={6} mb={2}>
            Lien de réinitialisation envoyé
          </Heading>
          <Text color={"gray.500"}>
            Un lien afin de réinitialiser votre mot de passe vous a été envoyé
            par mail.
          </Text>
          <Link color="blue.400" href="/connexion">
            Retourner à la page de connexion
          </Link>
        </Box>
      </Center>
    );
  }

  return (
    <ForgotPasswordView
      register={register}
      handleSubmit={handleSubmit}
      watch={watch}
      errors={errors}
      onSubmit={onSubmit}
      loading={isLoading}
      isError={isError}
      errorMessage={isErrorMessage}
    />
  );
};

export default ForgotPasswordController;
