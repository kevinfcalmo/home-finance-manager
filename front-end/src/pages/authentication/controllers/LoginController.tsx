import { useForm } from "react-hook-form";
import LoginView from "../views/LoginView";
import { useAuth } from "../../../context/userAuthContext";
import { useNavigate } from "react-router-dom";
import { useState } from "react";

const LoginController = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const authentication = useAuth();
  const [isLoading, setIsLoading] = useState(authentication?.loading);
  const [isError, setIsError] = useState(false);
  const [isErrorMessage, setIsErrorMessage] = useState(false);

  const navigate = useNavigate();
  const onSubmit = async (data: { email: string; password: string }) => {
    const login = await authentication?.login(data);
    setIsError(login.isError);
    setIsErrorMessage(login.message);
    if (!login.isError) {
      navigate("/");
    }
    console.log(login);
  };
  return (
    <LoginView
      register={register}
      handleSubmit={handleSubmit}
      watch={watch}
      errors={errors}
      onSubmit={onSubmit}
      loading={isLoading}
      isError={isError}
      errorMessage={isErrorMessage}
    />
  );
};

export default LoginController;
