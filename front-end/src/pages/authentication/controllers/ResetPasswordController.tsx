import ResetPasswordView from "../views/ResetPasswordView";
import { useNavigate, useSearchParams } from "react-router-dom";
import { CheckCircleIcon, CloseIcon } from "@chakra-ui/icons";
import { Box, Center, Flex, Heading, Link, Text } from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { useAuth } from "../../../context/userAuthContext";

const ResetPasswordController = () => {
  const [queryParameters] = useSearchParams();
  const token = queryParameters.get("token");

  if (!token) {
    return (
      <Center h="100vh">
        <Box textAlign="center" py={10} px={6}>
          <Box display="inline-block">
            <Flex
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
              bg={"red.500"}
              rounded={"50px"}
              w={"55px"}
              h={"55px"}
              textAlign="center"
            >
              <CloseIcon boxSize={"20px"} color={"white"} />
            </Flex>
          </Box>
          <Heading as="h2" size="xl" mt={6} mb={2}>
            Réinitialisation du mot de passe impossible
          </Heading>
          <Text color={"gray.500"}>
            Veuillez ressaissir le lien envoyé par email ou alors cliquer
            directement sur le lien afin de pouvoir réinitialiservotre mot de
            passe !
          </Text>
        </Box>
      </Center>
    );
  }

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();

  const authentication = useAuth();
  const navigate = useNavigate();

  const [isError, setIsError] = useState(false);
  const [isErrorMessage, setIsErrorMessage] = useState(false);
  const [isPasswordChange, setIsPasswordChange] = useState(false);

  const onSubmit = async (data: {
    password: string;
    confirmPassword: string;
  }) => {
    const resetPasswordRequest = await authentication?.resetPassword(
      data.password,
      data.confirmPassword,
      token
    );
    setIsError(resetPasswordRequest.isError);
    setIsErrorMessage(resetPasswordRequest.message);
    if (!resetPasswordRequest.isError) {
      setIsPasswordChange(true);
      setTimeout(() => {
        setIsPasswordChange(true);
        navigate("/connexion");
      }, 5000);
    }
  };

  if (isPasswordChange) {
    return (
      <Center h="100vh">
        <Box textAlign="center" py={10} px={6}>
          <CheckCircleIcon boxSize={"50px"} color={"green.500"} />
          <Heading as="h2" size="xl" mt={6} mb={2}>
            Mot de passe modifiée
          </Heading>
          <Text color={"gray.500"}>
            Votre nouveau mot de passe a bien été enregistré. Vous serez
            redirigé vers{" "}
            <Link href="/connexion" color="blue.400">
              la page de connexion
            </Link>{" "}
            dans 5secondes
          </Text>
        </Box>
      </Center>
    );
  }

  return (
    <ResetPasswordView
      register={register}
      handleSubmit={handleSubmit}
      watch={watch}
      errors={errors}
      onSubmit={onSubmit}
      loading={authentication!.loading}
      isError={isError}
      errorMessage={isErrorMessage}
    />
  );
};

export default ResetPasswordController;
