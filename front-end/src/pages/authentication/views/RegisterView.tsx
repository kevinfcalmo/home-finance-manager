"use client";

import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  InputGroup,
  HStack,
  InputRightElement,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  Link,
  Checkbox,
  Alert,
  AlertIcon,
} from "@chakra-ui/react";
import { useState } from "react";
import { FiEye, FiEyeOff } from "react-icons/fi";

export default function RegisterView({
  register,
  handleSubmit,
  watch,
  errors,
  onSubmit,
  loading,
  isError,
  errorMessage,
}: any) {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [isCheckboxCheck, setIsCheckboxCheck] = useState(false);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Flex
        minH={"100vh"}
        align={"center"}
        justify={"center"}
        bg={useColorModeValue("gray.50", "gray.800")}
      >
        <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
          {isError && (
            <Alert margin="20px 0" status="error">
              <AlertIcon />
              {errorMessage}
            </Alert>
          )}

          <Stack align={"center"}>
            <Heading fontSize={"4xl"} textAlign={"center"}>
              Inscription
            </Heading>
          </Stack>
          <Box
            rounded={"lg"}
            bg={useColorModeValue("white", "gray.700")}
            boxShadow={"lg"}
            p={8}
          >
            <Stack spacing={4}>
              <HStack>
                <Box>
                  <FormControl id="firstName" isRequired>
                    <FormLabel>Prénom</FormLabel>
                    <Input type="text" {...register("firstName")} />
                  </FormControl>
                </Box>
                <Box>
                  <FormControl id="lastName">
                    <FormLabel>Nom</FormLabel>
                    <Input type="text" {...register("lastName")} />
                  </FormControl>
                </Box>
              </HStack>
              <FormControl id="email" isRequired>
                <FormLabel>Adresse email</FormLabel>
                <Input type="email" {...register("email")} />
              </FormControl>
              <FormControl id="password" isRequired>
                <FormLabel>Mot de passe</FormLabel>
                <InputGroup>
                  <Input
                    type={showPassword ? "text" : "password"}
                    {...register("password")}
                  />
                  <InputRightElement h={"full"}>
                    <Button
                      variant={"ghost"}
                      onClick={() =>
                        setShowPassword((showPassword) => !showPassword)
                      }
                    >
                      {showPassword ? <FiEye /> : <FiEyeOff />}
                    </Button>
                  </InputRightElement>
                </InputGroup>
              </FormControl>
              <FormControl id="confirm-password" isRequired>
                <FormLabel>Confirmation mot de passe</FormLabel>
                <InputGroup>
                  <Input
                    type={showConfirmPassword ? "text" : "password"}
                    {...register("confirmPassword")}
                  />
                  <InputRightElement h={"full"}>
                    <Button
                      variant={"ghost"}
                      onClick={() =>
                        setShowConfirmPassword(!showConfirmPassword)
                      }
                    >
                      {showConfirmPassword ? <FiEye /> : <FiEyeOff />}
                    </Button>
                  </InputRightElement>
                </InputGroup>
              </FormControl>
              <FormControl>
                <Checkbox
                  onChange={() => {
                    setIsCheckboxCheck(!isCheckboxCheck);
                  }}
                >
                  J'accepte{" "}
                  <Link
                    _hover={{
                      color: "blue.600",
                    }}
                    textDecoration="underline"
                    color={"blue.400"}
                    href="#"
                  >
                    la politique de confidentialité et les CGU
                  </Link>
                </Checkbox>
              </FormControl>
              <Stack spacing={10} pt={2}>
                <Button
                  type="submit"
                  isDisabled={!isCheckboxCheck || loading}
                  isLoading={loading}
                  loadingText="En cours de création"
                  size="lg"
                  bg={loading ? "green.400" : "blue.400"}
                  color={"white"}
                  _hover={{
                    bg: "blue.500",
                  }}
                >
                  Je m'inscris
                </Button>
              </Stack>
              <Stack pt={6}>
                <Text align={"center"}>
                  Vous êtes déjà inscris ?{" "}
                  <Link color={"blue.400"} href="/connexion">
                    Connectez-vous !
                  </Link>
                </Text>
              </Stack>
            </Stack>
          </Box>
        </Stack>
      </Flex>
    </form>
  );
}
