"use client";

import {
  Button,
  FormControl,
  Flex,
  Heading,
  Input,
  Stack,
  Text,
  useColorModeValue,
  Alert,
  AlertIcon,
} from "@chakra-ui/react";

type ForgotPasswordFormInputs = {
  email: string;
};

export default function ForgotPasswordView({
  register,
  handleSubmit,
  watch,
  errors,
  onSubmit,
  loading,
  isError,
  errorMessage,
}: any) {
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Flex
        minH={"100vh"}
        align={"center"}
        justify={"center"}
        bg={useColorModeValue("gray.50", "gray.800")}
      >
        <Stack
          spacing={4}
          w={"full"}
          maxW={"md"}
          bg={useColorModeValue("white", "gray.700")}
          rounded={"xl"}
          boxShadow={"lg"}
          p={6}
          my={12}
        >
          {isError && (
            <Alert margin="20px 0" status="error">
              <AlertIcon />
              {errorMessage}
            </Alert>
          )}
          <Heading lineHeight={1.1} fontSize={{ base: "2xl", md: "3xl" }}>
            Mot de passe oublié?
          </Heading>
          <Text
            fontSize={{ base: "sm", sm: "md" }}
            color={useColorModeValue("gray.800", "gray.400")}
          >
            Vous recevrez un e-mail avec un lien de réinitialisation
          </Text>
          <FormControl id="email">
            <Input
              isRequired={true}
              placeholder="your-email@example.com"
              _placeholder={{ color: "gray.500" }}
              type="email"
              {...register("email")}
            />
          </FormControl>
          <Stack spacing={6}>
            <Button
              type="submit"
              bg={"blue.400"}
              color={"white"}
              _hover={{
                bg: "blue.500",
              }}
            >
              Demander une réinitialisation
            </Button>
          </Stack>
        </Stack>
      </Flex>
    </form>
  );
}
