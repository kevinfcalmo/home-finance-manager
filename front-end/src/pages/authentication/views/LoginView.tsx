"use client";

import {
  Flex,
  Box,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Button,
  Heading,
  Text,
  useColorModeValue,
  Link,
  Alert,
  AlertIcon,
} from "@chakra-ui/react";

export default function LoginView({
  register,
  handleSubmit,
  watch,
  errors,
  onSubmit,
  loading,
  isError,
  errorMessage,
}: any) {
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Flex
        minH={"100vh"}
        align={"center"}
        justify={"center"}
        bg={useColorModeValue("gray.50", "gray.800")}
      >
        <Stack spacing={8} mx={"auto"} maxW={"lg"} py={12} px={6}>
          <Stack align={"center"}>
            <Heading fontSize={"4xl"}>Connexion à votre compte</Heading>
          </Stack>
          <Box
            rounded={"lg"}
            bg={useColorModeValue("white", "gray.700")}
            boxShadow={"lg"}
            p={8}
          >
            {isError && (
              <Alert margin="20px 0" status="error">
                <AlertIcon />
                {errorMessage}
              </Alert>
            )}
            <Stack spacing={4}>
              <FormControl id="email">
                <FormLabel>Adresse e-mail</FormLabel>
                <Input
                  isDisabled={loading}
                  isInvalid={isError}
                  placeholder="johdoe@gmail.com"
                  type="email"
                  {...register("email")}
                />
              </FormControl>
              <FormControl id="password">
                <FormLabel>Mot de passe</FormLabel>
                <Input
                  isDisabled={loading}
                  isInvalid={isError}
                  placeholder="helloPassword"
                  type="password"
                  {...register("password")}
                />
              </FormControl>
              <Stack spacing={10}>
                <Stack
                  direction={{ base: "column", sm: "row" }}
                  align={"start"}
                  justify={"space-between"}
                >
                  {/* <Checkbox>Remember me</Checkbox> */}
                  <Link href="/mot-de-passe-oubliee" color={"blue.400"}>
                    Mot de passe oublié ?
                  </Link>
                </Stack>
                <Button
                  isLoading={loading}
                  loadingText="En cours de connexion"
                  type="submit"
                  bg={loading ? "green.400" : "blue.400"}
                  color={"white"}
                  _hover={{
                    bg: "blue.500",
                  }}
                >
                  Se connecter
                </Button>
              </Stack>
              <Stack pt={6}>
                <Text align={"center"}>
                  Pas encore de compte ?{" "}
                  <Link href="/inscription" color={"blue.400"}>
                    Inscris-toi juste ici !
                  </Link>
                </Text>
              </Stack>
            </Stack>
          </Box>
        </Stack>
      </Flex>
    </form>
  );
}
