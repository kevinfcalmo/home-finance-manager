import { useAuth } from "../../../context/userAuthContext";
import Layout from "../../../layouts/DefaultLayout";
import HomeView from "../views/HomeView";

const HomeController = () => {
  const authentication = useAuth();
  return (
    <Layout>
      <HomeView />
    </Layout>
  );
};

export default HomeController;
