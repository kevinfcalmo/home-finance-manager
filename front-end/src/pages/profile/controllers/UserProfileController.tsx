import { Badge, Stack, Text } from "@chakra-ui/react";
import SidebarWithHeader from "../../../layouts/DefaultLayout";
import { useAuth } from "../../../context/userAuthContext";

const UserProfileController = () => {
  const auth = useAuth();

  return (
    <SidebarWithHeader>
      <Stack>
        <Text>
          {auth?.cookies.firstName} {auth?.cookies.lastName}
        </Text>
        <Text>{auth?.cookies.email}</Text>
        <Text>
          Double authentification :{" "}
          {/* <Badge color="white" bgColor="blue.400">
            activé
          </Badge>{" "} */}
          <Badge color="white" bgColor="red.400">
            désactivé
          </Badge>
        </Text>
      </Stack>
    </SidebarWithHeader>
  );
};

export default UserProfileController;
