import UserProfileEdit from "../../../components/UserprofileEdit";
import SidebarWithHeader from "../../../layouts/DefaultLayout";

const UserProfileView = () => {
  return (
    <SidebarWithHeader>
      <UserProfileEdit />
    </SidebarWithHeader>
  );
};

export default UserProfileView;
