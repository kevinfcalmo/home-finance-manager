import {
  Navigate,
  Route,
  BrowserRouter as Router,
  Routes,
} from "react-router-dom";
import Login from "./pages/authentication/controllers/LoginController";
import { ProtectRoutes } from "./hooks/protectRoutes/protecRoutes";
import Home from "./pages/dashboard/controllers/HomeController";
import RegisterController from "./pages/authentication/controllers/RegisterController";
import ForgotPasswordController from "./pages/authentication/controllers/ForgotPasswordController";
import ResetPasswordController from "./pages/authentication/controllers/ResetPasswordController";
import UserProfileController from "./pages/profile/controllers/UserProfileController";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/connexion" element={<Login />} />
        <Route path="/inscription" element={<RegisterController />} />
        <Route
          path="/mot-de-passe-oubliee"
          element={<ForgotPasswordController />}
        />
        <Route
          path="/reinitialiser-mot-de-passe"
          element={<ResetPasswordController />}
        />
        <Route element={<ProtectRoutes />}>
          <Route path="/" element={<Home />} />
          <Route path="/profile" element={<UserProfileController />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
