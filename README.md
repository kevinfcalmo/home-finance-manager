# Home Finance Manager

HomeFinanceManager is a streamlined web app for managing household finances. It helps track and organize bills, rent, and other home expenses. Its user-friendly interface simplifies budgeting and financial oversight, making it easier to maintain control over household spending.

Project management : [Notion](https://charming-primula-058.notion.site/5bc38009c6494f41977b051d0a1dbd1d?v=2d69e13829ac49b5bf7c51ee7711a8ba&pvs=4)

## Author

[Kévin CALMO](https://www.kevincalmo.fr)

## Tech Stack

**Client:** React

**Server:** Node, Express
